Discriminating between Similar Languages
====
The Discriminating between Similar Languages (DSL) shared task aims to provide a dataset to evaluate system's performance on discriminating 13 different languages in 6 groups of languages:

* Group A (Bosnian, Croatian, Serbian)
* Group B (Indonesian, Malaysian)
* Group C (Czech, Slovakian)
* Group D (Brazilian Portuguese, European Portuguese)
* Group E (Peninsular Spain, Argentine Spanish)
* Group F (American English, British English)

Shared task webpage: [http://corporavm.uni-koeln.de/vardial/sharedtask.html](http://corporavm.uni-koeln.de/vardial/sharedtask.html)

For reproducibility, 

* the training data can be found: [https://db.tt/pRxy0IWW](https://db.tt/pRxy0IWW)
* the evaluation data can be found:[https://db.tt/av31jYYn](https://db.tt/av31jYYn)
* the gold data and the evaluation script can be found here: [https://db.tt/iLA6iY2D](https://db.tt/iLA6iY2D)

Participants
====
22 teams registered for the shared task and 7 teams submitted systems:

* **CLCG**: Johannes Bjerva - *University of Groningen*
* **LIRA**: Tiina Puolakainen - *University of Tartu*
* **NRC-CNRC**: Cyril Goutte, Serge Leger and Marine Carpuat - *National Research Council Canada*
* **QMUL**: Matthew Purver - *Queen Mary University of London*
* **RAE**: Jordi Porta and José Luis Sancho - *Centro de Estudios de la Real Academia Española*
* **UDE**: Torsten Zesch - *University of Duisburg-Essen*
* **UMich**: Ben King and Steven Abney - *University of Michigan*
* **UniMelb-NLP**: Marco Lui, Ned Letcher, Tim Baldwin, Paul Cook, Li Wang, Oliver Adams and Duong Thanh Long - *University of Melbourne* / *NICTA VRL*

Results
====
The overall results for the DSL shared task can be viewed [here](http://htmlpreview.github.io/?https://bitbucket.org/alvations/dslsharedtask2014/downloads/dsl-results.html)

The results for the individual teams can be found [here](http://goo.gl/WBuHtu)

Detailed results can be viewed [here](http://goo.gl/UGHxLi)

Organizers
====
* Marcos Zampieri, Saarland University
* Liling Tan, Saarland University
* Nikola Ljubešić, University of Zagreb
* Jörg Tiedemann, Uppsala University
